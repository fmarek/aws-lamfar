package com.bowcamp.lamfar.chrono;

public class Event {
    private String timeRequested;

    public Event() {
    }

    public Event(String timeRequested) {
        this.timeRequested = timeRequested;
    }

    public String getTimeRequested() {
        return timeRequested;
    }

    public void setTimeRequested(String timeRequested) {
        this.timeRequested = timeRequested;
    }
}
