package com.bowcamp.lamfar.chrono;

import com.amazonaws.services.ecs.AmazonECSClientBuilder;
import com.amazonaws.services.ecs.model.*;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class Main {
    public static void main(String args[]) {

        String frequency = System.getenv("frequency");
        String serviceType = System.getenv("serviceType");

        if (serviceType.equals("lambda")) {
            try {
                LambdaService service = LambdaInvokerFactory
                        .builder()
                        .lambdaFunctionNameResolver((method, annotation, config) -> "service_java_lambda_" + frequency)
                        .lambdaClient(AWSLambdaClientBuilder.defaultClient())
                        .build(LambdaService.class);

                service.invoke(new Event(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss").format(new Date())));

                System.out.println("OK");

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            RunTaskRequest req = new RunTaskRequest();
            req.setCluster("Cluster");
            req.setLaunchType("FARGATE");
            req.setTaskDefinition("service_java_standalone_" + frequency + ":2");
            req.setPlatformVersion("LATEST");
            req.setCount(1);
            req.setNetworkConfiguration(new NetworkConfiguration()
                    .withAwsvpcConfiguration(new AwsVpcConfiguration()
                            .withAssignPublicIp("ENABLED")
                            .withSubnets("subnet-bae457df")));
            req.setOverrides(
                    new TaskOverride()
                            .withContainerOverrides(new ContainerOverride()
                                    .withName("service_java_standalone")
                                    .withEnvironment(Arrays.asList(
                                            new KeyValuePair()
                                                    .withName("timeRequested")
                                                    .withValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss").format(new Date())),
                                            new KeyValuePair()
                                                    .withName("frequency")
                                                    .withValue(frequency.replaceAll("m", ""))
                                            )
                                    )));
            AmazonECSClientBuilder.standard().build().runTask(req);
        }

        System.exit(0);

    }
}
