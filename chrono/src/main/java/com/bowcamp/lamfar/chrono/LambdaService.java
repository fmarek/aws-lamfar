package com.bowcamp.lamfar.chrono;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface LambdaService {

    @LambdaFunction
    void invoke(Event event);

}
