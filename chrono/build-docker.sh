#!/bin/sh
cp build/libs/chrono-1.0.jar .
/usr/local/bin/docker rmi chrono:1.0
/usr/local/bin/docker build . -t chrono:1.0
/usr/local/bin/docker tag chrono:1.0 716745194950.dkr.ecr.us-west-2.amazonaws.com/lamfar:chrono_1.0