# Lambda and Fargate startup delays

## Purpose

Determine what are service invocation delays when service is packages as AWS Lambda function or AWS Fargate task.

## Report

Report is generated from stored events for various invocation frequencies.

[Lambda vs Fargate](https://infogram.com/lambda-vs-fargate-1hke60xdymye65r)

### Too long; Didn't read

* Average Lambda invocation delay fluctuates between 1 and 4 seconds.
* Average Fargate invocation delay fluctuates between 43 and 52 seconds, however with some extremes reaching 5+ minutes.

### Lambda

![Lambda](report-lambda.png "Lambda")

### Fargate

![Lambda](report-fargate.png "Fargate")

## Service & Model
Upon invocation service generates an event and stores in MongoDB. Event contains two time attributes - time when invocation request was sent and time when it was received by service instance.

```java
public class Event {

    private int frequency;
    private String timeRequested;
    private String timeReceived;
    private ServiceType type;

    ...

    public enum ServiceType {
        LAMBDA,
        FARGATE
    }
}
```

## Deployment

### As AWS Lambda

The following identical AWS Lambda functions have been created. The only difference is a configuration parameter 'frequency'

| Runtime | Name                     | Frequency Tag |
| --------|--------------------------|---------------|
| Java8   | service_java_lambda_m1   | m1            |
| Java8   | service_java_lambda_m5   | m5            |
| Java8   | service_java_lambda_m20  | m20           |
| Java8   | service_java_lambda_m60  | m60           |
| Java8   | service_java_lambda_m180 | m180          |
| Java8   | service_java_lambda_m360 | m360          |

### As AWS Fargate task

Service has been packaged as dockerised Java shell application. Then five identical fargate task definition have been created. The only difference is in the container's environment variable 'frequency'.

| Frequency Tag |
|---------------|
| m1            |
| m5            |
| m20           |
| m60           |
| m180          |

# Invocation

'Chrono' Java application has been created and scheduled via cron to invoke each lambda instance and run each fargate task. Invocation performed via AWS SDK for Java.

| Lambda Tag | Fargate Tag | Invocation frequency |
|------------|-------------|----------------------|
| m1         | m1          | every minute         |
| m5         | m5          | every 5 min          |
| m20        | m20         | every 20 min         |
| m60        | m60         | every hour           |
| m180       | m180        | every 3 hrs          |
| m360       |             | every 6 hrs          |

## Cron job invocation examples

```bash
# invoke Lambda
docker run --rm -e frequency=m5 -e serviceType=lambda lamfar:chrono_1.0
```

```bash
# invoke Fargate
docker run --rm -e frequency=m20 -e serviceType=fargate lamfar:chrono_1.0
```
## Experiment duration

Experiment had been running for about 52 hours
