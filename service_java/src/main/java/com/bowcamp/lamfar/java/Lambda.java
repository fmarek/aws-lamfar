package com.bowcamp.lamfar.java;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class Lambda implements RequestHandler<Event, String> {

    static final String DB_HOST = System.getenv("DB_HOST");
    static final String DB_NAME = System.getenv("DB_NAME");

    @Override
    public String handleRequest(Event input, Context context) {

        if (input.getTimeRequested() == null) return "No time requested";

        input.setFrequency(Integer.parseInt(System.getenv("frequency")));
        input.setType(Event.ServiceType.LAMBDA);

        new Service().work(input, DB_HOST, DB_NAME);

        return "OK";
    }
}
