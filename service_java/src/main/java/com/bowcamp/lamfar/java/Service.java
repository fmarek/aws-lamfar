package com.bowcamp.lamfar.java;

import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.mongodb.MongoClientSettings.builder;
import static com.mongodb.client.MongoClients.create;
import static java.util.Arrays.asList;

public class Service {

    public void work(Event event, String dbHost, String dbName) {
        event.setTimeReceived(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss").format(new Date()));

        MongoClient mongoClient = create(builder().applyToClusterSettings(builder -> builder.hosts(asList(new ServerAddress(dbHost)))).build());
        MongoDatabase database = mongoClient.getDatabase(dbName);
        MongoCollection<Document> events = database.getCollection("event");
        Document document = new Document("type", event.getType().name().toLowerCase())
                .append("impl", "java")
                .append("frequency", event.getFrequency())
                .append("timeRequested", event.getTimeRequested())
                .append("timeReceived", event.getTimeReceived());
        events.insertOne(document);
        mongoClient.close();

        try {
            Thread.sleep(10_000);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
