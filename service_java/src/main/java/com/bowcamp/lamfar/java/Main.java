package com.bowcamp.lamfar.java;

public class Main {
    public static void main(String args[]) {

        try {
            Event event = new Event();
            event.setTimeRequested(System.getenv("timeRequested"));
            event.setFrequency(Integer.parseInt(System.getenv("frequency")));
            event.setType(Event.ServiceType.FARGATE);

            new Service().work(event, System.getenv("DB_HOST"), System.getenv("DB_NAME"));

            System.out.println("OK");

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.exit(0);

    }
}
