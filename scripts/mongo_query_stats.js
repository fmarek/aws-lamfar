﻿var rs = ﻿db.getCollection('event').aggregate([
    {
        '$project': {
            'type': '$type', 'impl': '$impl', 'frequency': '$frequency',
            'timeRequested': '$timeRequested', 'timeReceived': '$timeReceived',
            'timeRequestedD': { '$dateFromString': { 'dateString': '$timeRequested' } },
            'timeReceivedD':  { '$dateFromString': { 'dateString': '$timeReceived' } },
            'duration': { '$subtract': [ '$timeReceivedD', '$timeRequestedD' ] }
         }
    },
    {
        '$project': {
            'type': '$type', 'impl': '$impl', 'frequency': '$frequency',
            'timeRequested': '$timeRequested', 'timeReceived': '$timeReceived',
            'duration': { '$subtract': [ '$timeReceivedD', '$timeRequestedD' ] }
         }
    },
    {
        '$group': {
            '_id': { 'type': '$type', 'impl': '$impl', 'frequency': '$frequency' },
            'avgDuration': { '$avg': '$duration' },
            'minDuration': { '$min': '$duration' },
            'maxDuration': { '$max': '$duration' }
         }
    },
    {
        '$project': {
            '_id': null,
            'type': '$_id.type', 'impl': '$_id.impl', 'frequency': '$_id.frequency',
            'avgDuration': { '$divide': ['$avgDuration', 1000] },
            'minDuration': { '$divide': ['$minDuration', 1000] },
            'maxDuration': { '$divide': ['$maxDuration', 1000] },
        }
    },
    {
        '$project': {
            'type': '$type', 'impl': '$impl', 'frequency': '$frequency',
            'avgDuration': { '$subtract':['$avgDuration', { '$mod': ['$avgDuration', 0.01]}] },
            'minDuration': { '$subtract':['$minDuration', { '$mod': ['$minDuration', 0.01]}] },
            'maxDuration': { '$subtract':['$maxDuration', { '$mod': ['$maxDuration', 0.01]}] }
        }
    },
    {
        $sort: { 'type': 1, 'frequency': 1 }
    }
]);

var read = function(rs) {
    var data = [];
    rs.forEach(function(row) { data.push(row); });
    return data;
}

print(JSON.stringify(read(rs), null, 2));